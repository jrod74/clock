public class Clock {
    // instance variables
    private int hour;
    private int minute;
    private int second;

    // tick() method changes the value of hour, minute, and second
    // void means nothing is returned
    public void tick() {
        this.second += 1;

        if (this.second >= 60) {
            this.minute += 1;
            this.second = 0;
        }
        if (this.minute >= 60) {
            this.hour += 1;
            this.minute = 0;
        }
        if (this.hour >= 24) {
            this.hour = 0;
        }

    }

    // Set default vale to true for twelveHourFormat
   // private boolean twelveHourFormat = true;

    // Derived Property, getter with no setter
    public String getCurrentTime() {
        return String.format("%02d:%02d:%02d", this.hour, this.minute, this.second);
      /*  return this.hour + ":" + this.minute + ":" + this.second;

        // Make use of the twelveHourFormat property
        String currentTime = ":" + this.minute + ":" + this.second;
        if (this.twelveHourFormat) {
            if (this.hour > 12) {
                currentTime = (this.hour - 12) + currentTime + " pm";
            } else if (this.hour == 0) {
                currentTime = "12" + currentTime + " am";
            } else {
                currentTime = this.hour + currentTime + " am";
            }
        } else {
            currentTime = this.hour + currentTime;
        }
        return currentTime;

       */
    }


    //setter
    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public void setSecond(int second) {
        this.second = second;
    }

  //  public void setTwelveHourFormat(boolean twelveHourFormat) {
     //   this.twelveHourFormat = twelveHourFormat;
  //  }


    // getter
    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public int getSecond() {
        return second;
    }

 //   public boolean isTwelveHourFormat() {
  //     return this.twelveHourFormat;
  //  }
}
