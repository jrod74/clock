public class Program {
    // program. java
    public static void main(String[] args) {

        // Instantiate a new instance of a clock
        Clock grandfather = new Clock();

        // set the time to 17:22:32

        grandfather.setHour(12);
        grandfather.setMinute(22);
        grandfather.setSecond(32);
       // twelveHourFormat default to true

        grandfather.tick();// time will be 12:22:33
        grandfather.tick(); // time wil be 12:22:34

         //Print "Current time is 5:22:32
        System.out.println("Current time is " + grandfather.getCurrentTime());

        // set the time to 20:36:15
/*
        Clock humveeDashboard = new Clock();
        humveeDashboard.setHour(20);
        humveeDashboard.setMinute(36);
        humveeDashboard.setSecond(15);
        // Override twelveHourFormat default, and set to false
        humveeDashboard.setTwelveHourFormat(false);

 */
        //Print "Current time is 20:36:15
           // System.out.println("Current time is " + humveeDashboard.getCurrentTime());
    }
}
